import speech_recognition as sr
import time

class ASR():

    def __init__(self):
        self.recording = sr.Recognizer()
        self.recording.energy_threshold = 4000
        #
        # self.lastMessage = ""

    def listen(self, wait = 1):
        with sr.Microphone() as source:
            if wait>0:
                self.recording.adjust_for_ambient_noise(source, wait) # wait for seconds
            print("start listening")
            audio = self.recording.listen(source, 5, 14)
            self.callback(audio)

            # stop_listening = self.recording.listen_in_background(source, self.callback)
            #
            # time.sleep(2)
            # stop_listening()
            # print("stop listening")

            return self.lastMessage

    def callback(self, audio):
        self.lastMessage = ""
        try:
            # print("audio")
            self.lastMessage = self.recording.recognize_google(audio, None, 'cs-CZ')
        except sr.UnknownValueError:
            print("Google Speech Recognition could not understand audio")
        except sr.RequestError as e:
            print("Could not request results from Google Speech Recognition service; {0}".format(e))