# ufal-nao-demo

## Jak vse funguje

Nao neprovadi zadne vypocty a vicemene funguje pouze jako reproduktor. Vse obstarava pocitac v pozadi.

Nao bezi jako server a posloucha, kdy se k nemu pripoji pocitac a posle mu audio, ktere ma nao prehrat.

V pocitaci bezi 2 procesy: API s politikem; python script ktery propojuj ASR, TTS, politika a NAO 

K prehravani v hlucnem prostoru je dobre mit reproduktory, protoze NAO je dost tichy. Stejne tak mikrofon v pocitaci by mel byt pripraveny na dane podminky (napr. pujcit externi)


## Priprava

V pocitaci musi byt nainstalovan Choreograph, python a perl.

Choreographe licence key: 654e-4564-153c-6518-2f44-7562-206e-4c60-5f47-5f45

Do pythonu nainstaluj knihovny z requirements.txt

Perl bude potrebovat instalovat nekolik knihovem. Nejlepsi je vyuzit postup zde: https://ufal.mff.cuni.cz/treex/install.html

-- Ja jsem mel problem s nejakyma knihovnama a potom Treex jsem si musel vzit z githubu nejnovejsi

## Propojeni

NAO a pocitac museji byt na stejne wifi (nyni NAO bezel na MS-guest). Pokud nejsou, je to potreba nakonfigujovat pres LAN kabel a webove rozhrani robota.

### Pripojeni k webovemu rozhrani

Stiskem hrudniho tlacitka robot nadiktuje adresu

Jmeno: nao

Heslo: nao

Nastavit stejnou wifi

## Spusteni chovani robota

V choreographu pripoj NAO robota (zelene tlacitko s wifi). Pokud je na stejne wifi a nejde najit, lze pripojit zadanim IP adresy primo.

Nacti zdrojovy kod z UFAL-NAO-robot-behaviour a spust. (v behavior "listen to port" je potreba nastavit IP adresu robota a pri padu menit port ... stejne nastaveni musi byt i v main.py)

V pocitaci spust politician/api_humphrey.pl, ktery bude poslouchat na portu 7777 na otazky.

V pocitaci spust main.py

## Pouzivani

Jakmile je vse propojene, tak v pocitaci skript ceka na zmacknuti ENTER aby zacal poslouchat (to je omezeni, aby NAO neodpovidal na um).

Ja jsem mel v ruce ukazovatko, kterym jsem to spoustel.

Po stisknuti NAO 1 sekundu posloucha na sum a pak az zacne rozpoznavat speech. Jak k ASR i TTS vyuzivam Google API, takze bez internetu to nebue fungovat.

## Caste problemy

Pokud chovani v robotovi z nejakeho duvodu spadne, tak nelze nahrat znovu behaviour na robota, je potreba zmenit port na kterem poslocha jak v robotovi tak v main.py (v robotovi se to dela v krabicce server)


### POZNAMKY PRO BUDOUCI VYVOJ - nejsou uzitecne nyni

Chce to pridat moznost vypnuti nahravani zvuku protoze hrozne dlouho ceka nez prestane poslouchat



Plzen potrebuje https://www.pjsip.org/download.htm

Tutorial for NAO - notes

!!! In this demo it is not used !!!

Needs only python 2.7

It runs on Ubuntu 14.04 ... if you have nower version you need to downgrade boost na 1.55

https://www.boost.org/doc/libs/1_55_0/more/getting_started/unix-variants.html


Instalation:

Instal naoqi: http://doc.aldebaran.com/1-14/dev/python/install_guide.html#python-install-guide

pip install -r requirements.txt
