import socket
import sys


class Request():

    def __init__(self, ip, port):
        self.ip = ip
        self.port = port

    def initialize(self):
        # Create a TCP/IP socket
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        # Connect the socket to the port where the server is listening
        server_address = (self.ip, self.port)
        sys.stderr.write('connecting to %s port %s' % server_address)
        self.sock.connect(server_address)

    def recvall(self):
        BUFF_SIZE = 1024
        data = b''
        while True:
            part = self.sock.recv(BUFF_SIZE)
            data += part
            if len(part) < BUFF_SIZE:
                # either 0 or end of data
                break
        return data

    def callApi(self, message):
        self.initialize()
        try:
            # Send data
            self.sock.sendall(message.encode('utf-8'))

            data = self.recvall()
        except Exception as e:
            sys.stderr.write(e)
        finally:
            self.sock.close()

        return data.decode()
