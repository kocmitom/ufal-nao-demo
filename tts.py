from gtts import gTTS
import pyglet
from time import sleep
from pydub import AudioSegment

AudioSegment.converter = "C:\\FFmpeg\\bin\\ffmpeg.exe"
AudioSegment.ffmpeg = "C:\\FFmpeg\\bin\\ffmpeg.exe"
AudioSegment.ffprobe ="C:\\FFmpeg\\bin\\ffprobe.exe"

class TTS():

    def __init__(self):
        self.tempfile = 'temp.mp3'
        self.finalfile = 'temp_louder.mp3'
        self.counter = 0

    def msg2speech(self, message):
        tts = gTTS(message, 'cs')

        tts.save(self.finalfile)

        # tts.save(self.tempfile)
        # song = AudioSegment.from_mp3(self.tempfile)
        # # boost volume by 6dB
        # louder_song = song + 10
        # louder_song.export(self.finalfile, format='mp3')

        # convert to wav
        # sound = AudioSegment.from_mp3(filename)
        # sound.export(filenamewav, format="wav")

        self.counter += 1

    def preload(self):
        self.music = pyglet.media.load(self.finalfile, streaming=False)

    def playPreloaded(self):
        self.music.play()
        sleep(self.music.duration) #prevent from killing


    def playLastSpeech(self):
        music = pyglet.media.load(self.finalfile, streaming=False)
        music.play()
        sleep(music.duration) #prevent from killing

    def waitLengthOfSpeech(self):
        music = pyglet.media.load(self.finalfile, streaming=False)
        sleep(music.duration)  # prevent from killing







if __name__== "__main__":
    from politician import Politician
    from asr import ASR

    polit = Politician()
    asr = ASR()
    tts = TTS()
    while True:
        input("Press Enter to continue...")
        question = "Co si myslíte o parlamentu ˇ$eské Republiky?"
        question = asr.listen()
        print("Question:", question)
        answer = polit.askQuestion(question + "?") # hack protože TTS nevraci otazník
        print("Answer:", answer)
        tts.msg2speech(answer)
        # tts.waitLengthOfSpeech()
        tts.playLastSpeech()