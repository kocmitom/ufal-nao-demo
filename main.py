#!/usr/bin/env python
# -*- coding: utf-8 -*-

import socket
import sys
from tts import TTS
from threading import Thread
from politician import Politician
from asr import ASR
import time

tts = TTS()

lastEnter = 0
polit = Politician()
asr = ASR()
while True:
    input("Press Enter to continue...")
    while lastEnter>int(round(time.time() * 1000))-50: # wait some time in millisecunds
        print("muliple inputs")
        continue
    question = asr.listen(0)
    if(question is None or len(question)<10):
        print("INFO: too short question")
        continue
    # if(question is None):
    #     print("INFO: empty question")
    #     continue
    print("Question:", question)
    answer = polit.askQuestion(question + "?")  # hack protože TTS nevraci otazník
    print("Answer:", answer)
    tts.msg2speech(answer)

    tts.preload()
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_address = ('10.10.7.31', 8874)
    print('connecting ...')
    sock.connect(server_address)

    try:
        f = open(tts.finalfile, 'rb')

        BUFF_SIZE = 1024
        l = f.read(BUFF_SIZE)
        while (l):
            # print('Sending...')
            sock.send(l)
            l = f.read(BUFF_SIZE)
        f.close()

        # time.sleep(0.1)

    finally:
        print('closing socket')
        sock.close()

    tts.playPreloaded()
    # tts.playLastSpeech()
    # tts.waitLengthOfSpeech()

    lastEnter = int(round(time.time() * 1000))


